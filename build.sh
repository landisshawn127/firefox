echo "packing $CI_PROJECT_PATH"

build() {
    pip install -r requirements.txt

    echo "Running script"
    python3 build.py || NEW_VERSION=1
}

commit() {
    echo "Committing new version"
    git checkout dev
    git add pkg.yml && git commit -m "New Version detected"
    git push https://$DEPLOY_USER:$DEPLOY_TOKEN@gitlab.com/$CI_PROJECT_PATH.git
}

pack() {
    echo "Running Packer"
    xpacker p
    xpacker reg p -t gitlab -a $DEPLOY_TOKEN
}

echo "Installing Requirements"

NEW_VERSION=0

build


if [ "$NEW_VERSION" -eq "0" ]; then
    echo "New Version detected";
    pack
    commit
else
    echo "No New Version";
fi
